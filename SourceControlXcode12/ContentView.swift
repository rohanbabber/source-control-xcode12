//
//  ContentView.swift
//  SourceControlXcode12
//
//  Created by Rohan Babber on 24/08/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world! Xcode 12 Branch")
            .padding()
            .font(.title)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

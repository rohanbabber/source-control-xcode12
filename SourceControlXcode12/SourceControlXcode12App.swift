//
//  SourceControlXcode12App.swift
//  SourceControlXcode12
//
//  Created by Rohan Babber on 24/08/21.
//

import SwiftUI

@main
struct SourceControlXcode12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
